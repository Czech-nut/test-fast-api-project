from fastapi import FastAPI, Depends
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from sqlalchemy.orm import Session

import models
from models import User, Address
from database import SessionLocal, engine


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
    allow_credentials=True,
)

models.Base.metadata.create_all(bind=engine)


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


class UserRequest(BaseModel):
    name: str
    surname: str
    phone: int


class AddressRequest(BaseModel):
    city: str
    street: str
    building: int
    apartment: int


@app.post("/user", status_code=201)
def create_user(request: UserRequest, db: Session = Depends(get_db)):
    user = User()
    user.name = request.name
    user.surname = request.surname
    user.phone = request.phone
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


@app.post("/address", status_code=201)
def create_address(request: AddressRequest, db: Session = Depends(get_db)):
    address = Address()
    address.city = request.city
    address.street = request.street
    address.building = request.building
    address.apartment = request.apartment
    db.add(address)
    db.commit()
    db.refresh(address)
    return address


@app.post("/address/{address_id}/set_user/{user_id}", status_code=200)
def set_user_to_address(address_id: int, user_id: int, db: Session = Depends(get_db)):
    address = db.query(Address).filter(Address.id == address_id).first()
    user = db.query(User).filter(User.id == user_id).first()
    address.users.append(user)
    db.commit()


@app.get("/user/{user_id}/addresses", status_code=200)
def get_user_addresses(user_id: int, db: Session = Depends(get_db)):
    user = db.query(User).filter(User.id == user_id).first()
    return user.addresses


@app.get("/address/{address_id}/users", status_code=200)
def get_address_users(address_id: int, db: Session = Depends(get_db)):
    address = db.query(Address).filter(Address.id == address_id).first()
    return address.users
