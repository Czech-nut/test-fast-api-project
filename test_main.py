from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


USER_BODY = {
    'name': 'Sherlock',
    'surname': 'Holmes',
    'phone': 1234567
}

ADDRESS_BODY = {
    'city': 'London',
    'street': 'Baker',
    'building': 221,
    'apartment': 1
}


def test_create_user():
    response = client.post("/user", json=USER_BODY)
    assert response.status_code == 201


def test_create_address():
    response = client.post("/address", json=ADDRESS_BODY)
    assert response.status_code == 201


def test_set_user_to_address():
    user_id = client.post("/user", json=USER_BODY).json().get('id')
    address_id = client.post("/address", json=ADDRESS_BODY).json().get('id')
    response = client.post(f"/address/{address_id}/set_user/{user_id}")
    assert response.status_code == 200


def test_get_user_addresses():
    user = client.post("/user", json=USER_BODY).json()
    address = client.post("/address", json=ADDRESS_BODY).json()
    client.post(f"/address/{address.get('id')}/set_user/{user.get('id')}")
    response = client.get(f"/user/{user.get('id')}/addresses")
    assert response.status_code == 200


def test_get_address_users():
    user = client.post("/user", json=USER_BODY).json()
    address = client.post("/address", json=ADDRESS_BODY).json()
    client.post(f"/address/{address.get('id')}/set_user/{user.get('id')}")
    response = client.get(f"/address/{address.get('id')}/users")
    assert response.status_code == 200
