from behave import *
import requests


@given('we have a user')
def step_impl(context):
    request_body = {
        'name': 'Sherlock',
        'surname': 'Holmes',
        'phone': 1234567
    }
    response = requests.post('http://127.0.0.1:8000/user', json=request_body)
    context.user = response.json()


@given('we have 2 addresses')
def step_impl(context):
    address_data_1 = {
        'city': 'London',
        'street': 'Baker',
        'building': 221,
        'apartment': 1
    }
    address_data_2 = {
        'city': 'London',
        'street': 'Baker',
        'building': 222,
        'apartment': 2
    }
    address_1 = requests.post('http://127.0.0.1:8000/address', json=address_data_1)
    address_2 = requests.post('http://127.0.0.1:8000/address', json=address_data_2)
    context.address_1 = address_1.json()
    context.address_2 = address_2.json()


@when('we set the user to the address')
def step_impl(context):
    requests.post(f'http://127.0.0.1:8000/address/{context.address_1.get("id")}/set_user/{context.user.get("id")}')
    requests.post(f'http://127.0.0.1:8000/address/{context.address_2.get("id")}/set_user/{context.user.get("id")}')


@then('address has user in its users list and responds with status code 200')
def step_impl(context):
    response = requests.get(f'http://127.0.0.1:8000/user/{context.user.get("id")}/addresses').json()
    print(response)
    assert len(response) == 2
