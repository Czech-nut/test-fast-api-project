Feature: set user to address

  Scenario: set user to address
     Given we have a user
       And we have 2 addresses
      When we set the user to the address
      Then address has user in its users list and responds with status code 200
